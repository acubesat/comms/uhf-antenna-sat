# On-board UHF Antenna

This repository contains electromagnetic antenna simulation files of the AcubeSAT on-board dipole and turnstile UHF anetnnas. As of 2020-12-24, the most up-to-date model for the dipole can be found in [`UHF_dipole_horizontal.cst`](https://gitlab.com/acubesat/comms/uhf-antenna-sat/-/blob/master/dipole/UHF_dipole_horizontal.cst).

As of 2021-05-18, the most up-to-date model for the dipole can be found in [turnstile.aedt](https://gitlab.com/acubesat/comms/uhf-antenna-sat/-/blob/master/Turnstile/turnstile.aedt)

## Documentation

Dipole simulation results can be found in the [AcubeSAT-COM-G-025](https://helit.org/mm/docList/public/AcubeSAT-COM-G-025) technical report.

Turnstile simulation results can be found [here](https://gitlab.com/acubesat/comms/uhf-antenna-sat/-/tree/master/Turnstile/Sim_results)
